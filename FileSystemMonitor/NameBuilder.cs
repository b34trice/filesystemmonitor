﻿using Data;
using System;

namespace FileSystemMonitor
{
    public class NameBuilder : INameBuilder
    {
        private readonly bool _isAddNumber;
        private readonly bool _isAddDateTime;

        public NameBuilder(bool isAddNumber, bool isAddDateTime)
        {
            _isAddDateTime = isAddDateTime;
            _isAddNumber = isAddNumber;
        }

        public string GetFinalName(string originalPath, int totalFilesCounter)
        {
            var nameParts = originalPath.Split('.');
            var resultName = nameParts[0];
            if (_isAddDateTime)
            {
                resultName = $"{resultName}-{DateTime.Now.ToString("dd.MM.yyyy")}";
            }
            if (_isAddNumber)
            {
                resultName = $"{resultName}-{totalFilesCounter}";
            }
            return $"{resultName}.{nameParts[^1]}";
        }
    }
}