﻿using Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;

namespace FileSystemMonitor
{
    internal class Program
    {
        private static readonly ILogger _logger;
        private static readonly INameBuilder _pathBuilder;
        private static readonly IRuleChecker _ruleChecker;

        private static readonly Dictionary<Regex, string> _rules = new Dictionary<Regex, string>();
        private static readonly List<string> _folders = new List<string>();
        private static string _defaultFolder;
        private static bool _isAddElementNumber;
        private static bool _isAddMoveDateTime;

        static Program()
        {
            InitSettings(AppConfig.GetConfig());
            _logger = new Logger.Core.Logger();
            _pathBuilder = new NameBuilder(_isAddElementNumber, _isAddMoveDateTime);
            _ruleChecker = new RuleChecker(_pathBuilder, _defaultFolder);
        }

        private static void Main(string[] args)
        {
            new FileMonitor(_logger, _ruleChecker)
                .StartWatch(_rules, _folders, _defaultFolder, _isAddMoveDateTime, _isAddElementNumber);
            Console.ReadKey();
        }

        private static void InitSettings(AppConfig config)
        {
            foreach (var item in config.FoldersRules)
            {
                var rule = item as FolderRule;
                var path = rule.Path;
                if (Directory.Exists(path))
                {
                    _rules.Add(new Regex(@$"{rule.Regex}"), path);
                }
            }

            foreach (var folder in config.FoldersWatch)
            {
                var path = (folder as FolderWatch).Path;
                if (Directory.Exists(path))
                {
                    _folders.Add(path);
                }
            }

            var local = config.Local.LocalName;
            Thread.CurrentThread.CurrentCulture = new CultureInfo(local);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(local);

            _defaultFolder = config.DefaultFolder.Path;
            if (!Directory.Exists(_defaultFolder))
            {
                throw new Exception("Default folder is not valid");
            }

            _isAddElementNumber = config.ElementNumber.IsEnable;
            _isAddMoveDateTime = config.MoveDate.IsEnable;
        }
    }
}