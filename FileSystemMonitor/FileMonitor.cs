﻿using Data;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Timers;
using messages = FileSystemMonitor.Resources.Logs;

namespace FileSystemMonitor
{
    internal class FileMonitor : IFileMonitor
    {
        private const int TIMER_WATCH_INTERVAL = 10000;

        private readonly ILogger _logger;
        private readonly IRuleChecker _ruleChecker;

        private readonly Timer _timer;

        private IEnumerable<string> _folders;
        private Dictionary<Regex, string> _rules;
        private int _totalFilesCounter = 0;

        public FileMonitor(
            ILogger logger,
            IRuleChecker ruleChecker)
        {
            _logger = logger;
            _ruleChecker = ruleChecker;
            _timer = new Timer() { AutoReset = true, Interval = TIMER_WATCH_INTERVAL };
        }

        public void StartWatch(
            Dictionary<Regex, string> rules,
            IEnumerable<string> folders,
            string defaultFolder,
            bool isAddDateTime,
            bool isAddNumber)
        {
            _folders = folders;
            _rules = rules;
            _timer.Elapsed += OnWatchInvoke;
            _timer.Start();
            _logger.Log(messages.StartMessage);
        }

        private void OnWatchInvoke(object sender, ElapsedEventArgs e)
        {
            foreach (var folder in _folders)
            {
                var directoryInfo = new DirectoryInfo(folder);
                var files = directoryInfo.GetFiles();

                foreach (var file in files)
                {
                    _logger.Log($"{messages.NewFileMessage}.{file.Name}.{file.CreationTime}");
                    CheckFile(file);
                    _totalFilesCounter++;
                }
            }
        }

        private void CheckFile(FileInfo file)
        {
            var name = file.Name;
            foreach (var rule in _rules)
            {
                HandleFile(_ruleChecker.GetData(rule, name, _totalFilesCounter), file.FullName);
                break;
            }
        }

        private void HandleFile(FileData fileData, string originalPath)
        {
            _logger.Log($"{fileData.Message} - {fileData.Path} ");
            CopyAndDelete(originalPath, fileData.Path); ;
        }

        private void CopyAndDelete(string source, string destination)
        {
            _logger.Log(messages.CopyFileMessage);
            File.Copy(source, destination, true);
            _logger.Log(messages.DeleteFileMessage);
            File.Delete(source);
            _logger.Log(messages.FinishMessage);
        }
    }
}