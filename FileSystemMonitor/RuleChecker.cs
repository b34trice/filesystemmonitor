﻿using Data;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using messages = FileSystemMonitor.Resources.Logs;

namespace FileSystemMonitor
{
    public class RuleChecker : IRuleChecker
    {
        private readonly INameBuilder _nameBuilder;
        private readonly string _defaultFolder;

        public RuleChecker(INameBuilder nameBuilder, string defaultFolder)
        {
            _nameBuilder = nameBuilder;
            _defaultFolder = defaultFolder;
        }

        public FileData GetData(
            KeyValuePair<Regex, string> rule,
            string name,
            int totalFilesCounter)
        {
            string destinationPath;
            string message;
            var finalName = _nameBuilder.GetFinalName(name, totalFilesCounter);
            if (rule.Key.IsMatch(name))
            {
                destinationPath = $"{ rule.Value }\\{finalName}";
                message = messages.ExistsRegexMessage;
            }
            else
            {
                destinationPath = $"{_defaultFolder}\\{finalName}";
                message = $"{messages.RegexNotExistsMessage} - {destinationPath} ";
            }

            return new FileData { Path = destinationPath, Message = message };
        }
    }
}