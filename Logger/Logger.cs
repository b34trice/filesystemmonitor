﻿using Data;
using System;

namespace Logger.Core
{
    public class Logger : ILogger
    {
        public void Log(object message)
        {
            Console.WriteLine(message);
        }
    }
}
