﻿namespace Data
{
    public interface INameBuilder
    {
        string GetFinalName(string originalPath, int totalFilesCounter);
    }
}