﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Data
{
    public interface IFileMonitor
    {
        void StartWatch(
            Dictionary<Regex, string> rules,
            IEnumerable<string> folders,
            string defaultFolder,
            bool isAddDateTime,
            bool isAddNumber);
    }
}