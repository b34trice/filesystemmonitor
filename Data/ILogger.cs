﻿namespace Data
{
    public interface ILogger
    {
        void Log(object message);
    }
}