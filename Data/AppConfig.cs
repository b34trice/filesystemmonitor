﻿using System.Configuration;

namespace Data
{
    public class AppConfig : ConfigurationSection
    {
        public static AppConfig GetConfig()
        {
            return (AppConfig)ConfigurationManager.GetSection("AppSettings") ?? new AppConfig();
        }

        [ConfigurationProperty("FoldersRules")]
        [ConfigurationCollection(typeof(FoldersRules), AddItemName = "FolderRule")]
        public FoldersRules FoldersRules
        {
            get => this["FoldersRules"] as FoldersRules;
        }

        [ConfigurationProperty("FoldersWatch")]
        [ConfigurationCollection(typeof(FoldersWatch), AddItemName = "FolderWatch")]
        public FoldersWatch FoldersWatch
        {
            get => this["FoldersWatch"] as FoldersWatch;
        }

        [ConfigurationProperty("MoveDate")]
        public MoveDate MoveDate
        {
            get => this["MoveDate"] as MoveDate;
        }

        [ConfigurationProperty("DefaultFolder")]
        public DefaultFolder DefaultFolder
        {
            get => this["DefaultFolder"] as DefaultFolder;
        }

        [ConfigurationProperty("Local")]
        public Local Local
        {
            get => this["Local"] as Local;
        }

        [ConfigurationProperty("ElementNumber")]
        public ElementNumber ElementNumber
        {
            get => this["ElementNumber"] as ElementNumber;
        }
    }
}
