﻿namespace Data
{
    public struct FileData
    {
        public string Message { get; set; }

        public string Path { get; set; }
    }
}