﻿using System.Configuration;

namespace Data
{
    public class FoldersRules : ConfigurationElementCollection
    {
        public FolderRule this[int index]
        {
            get => BaseGet(index) as FolderRule;
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        public new FolderRule this[string responseString]
        {
            get => (FolderRule)BaseGet(responseString);
            set
            {
                if (BaseGet(responseString) != null)
                {
                    BaseRemoveAt(BaseIndexOf(BaseGet(responseString)));
                }
                BaseAdd(value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new FolderRule();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((FolderRule)element).Regex;
        }
    }
}
