﻿using System.Configuration;

namespace Data
{
    public class Local: ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string LocalName => this["name"] as string;
    }
}
