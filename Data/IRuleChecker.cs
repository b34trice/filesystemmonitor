﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Data
{
    public interface IRuleChecker
    {
        FileData GetData(KeyValuePair<Regex, string> rule,
                    string name,
                    int totalFilesCounter);
    }
}