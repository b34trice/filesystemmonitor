﻿using System.Configuration;

namespace Data
{
    public class FoldersWatch : ConfigurationElementCollection
    {
        public FolderWatch this[int index]
        {
            get => BaseGet(index) as FolderWatch;
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        public new FolderWatch this[string responseString]
        {
            get => (FolderWatch)BaseGet(responseString);
            set
            {
                if (BaseGet(responseString) != null)
                {
                    BaseRemoveAt(BaseIndexOf(BaseGet(responseString)));
                }
                BaseAdd(value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new FolderWatch();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((FolderWatch)element).Path;
        }
    }
}
