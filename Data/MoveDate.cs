﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace Data
{
    public class MoveDate: ConfigurationElement
    {
        [ConfigurationProperty("isEnable", IsRequired = true)]
        public bool IsEnable => (bool)this["isEnable"];
    }
}
