﻿using System.Configuration;

namespace Data
{
    public class FolderWatch:ConfigurationElement
    {
        [ConfigurationProperty("path", IsRequired = true)]
        public string Path => this["path"] as string;
    }
}
