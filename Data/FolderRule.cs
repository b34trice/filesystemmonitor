﻿using System.Configuration;

namespace Data
{
    public class FolderRule : ConfigurationElement
    {
        [ConfigurationProperty("regex", IsRequired = true)]
        public string Regex => this["regex"] as string;

        [ConfigurationProperty("path", IsRequired = true)]
        public string Path => this["path"] as string;
    }
}