﻿using System.Configuration;

namespace Data
{
    public class ElementNumber: ConfigurationElement
    {
        [ConfigurationProperty("isEnable", IsRequired = true)]
        public bool IsEnable => (bool)this["isEnable"];
    }
}
