﻿using FileSystemMonitor;
using System;
using Xunit;

namespace FileSystemMonitorUnitTests
{
    public class NameBuilderUnitTests
    {
        public class GetFinalNameTests : NameBuilderUnitTests
        {
            [Theory]
            [InlineData("test.txt", true, false, 0)]
            public void GetFinalName_GetNameWithOnlyDate(string fileName, bool isAddDateTime, bool isAddNumber, int fileCounter)
            {
                var service = new NameBuilder(isAddNumber, isAddDateTime);
                var expectedResult = $"test-{DateTime.Now.ToString("dd.MM.yyyy")}.txt";
                var actualResult = service.GetFinalName(fileName, fileCounter);
                Assert.Equal(expectedResult, actualResult);
            }

            [Theory]
            [InlineData("test.txt", false, true, 0)]
            public void GetFinalName_GetNameWithOnlyNumber(string fileName, bool isAddDateTime, bool isAddNumber, int fileCounter)
            {
                var service = new NameBuilder(isAddNumber, isAddDateTime);
                var expectedResult = $"test-{fileCounter}.txt";
                var actualResult = service.GetFinalName(fileName, fileCounter);
                Assert.Equal(expectedResult, actualResult);
            }

            [Theory]
            [InlineData("test.txt", true, true, 0)]
            public void GetFinalName_GetNameWithDateAndNumber(string fileName, bool isAddDateTime, bool isAddNumber, int fileCounter)
            {
                var service = new NameBuilder(isAddNumber, isAddDateTime);
                var expectedResult = $"test-{DateTime.Now.ToString("dd.MM.yyyy")}-{fileCounter}.txt";
                var actualResult = service.GetFinalName(fileName, fileCounter);
                Assert.Equal(expectedResult, actualResult);
            }

            [Theory]
            [InlineData("test.txt", false, false, 0)]
            public void GetFinalName_GetNameWithoutModification(string fileName, bool isAddDateTime, bool isAddNumber, int fileCounter)
            {
                var service = new NameBuilder(isAddNumber, isAddDateTime);
                var expectedResult = $"test.txt";
                var actualResult = service.GetFinalName(fileName, fileCounter);
                Assert.Equal(expectedResult, actualResult);
            }
        }
    }
}