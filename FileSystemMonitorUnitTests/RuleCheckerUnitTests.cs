﻿using Data;
using FileSystemMonitor;
using Moq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Xunit;

namespace FileSystemMonitorUnitTests
{
    public class RuleCheckerUnitTests
    {
        private const string DEFAULT_FOLDER_PATH = "C:\\default\\";
        private const string RULE_EXISTS_MESSAGE = "Detected regex rule. Move to path";

        private readonly string RULE_NOT_EXISTS_MESSAGE = $"Regex rule not found. Move to default path - {DEFAULT_FOLDER_PATH}";
        private readonly IRuleChecker _service;
        private readonly Mock<INameBuilder> _nameBuilder;


        public RuleCheckerUnitTests()
        {
            _nameBuilder = new Mock<INameBuilder>();
            _service = new RuleChecker(_nameBuilder.Object, DEFAULT_FOLDER_PATH);
        }

        public class GetDataTests : RuleCheckerUnitTests
        {
            [Theory]
            [InlineData("test.txt", "txt$", "C:\\temp")]
            public void GetData_RuleExists(string fileName, string regex, string destinationFolder)
            {
                _nameBuilder.Setup(s => s.GetFinalName(It.IsAny<string>(), It.IsAny<int>())).Returns(fileName);
                var rule = new KeyValuePair<Regex, string>(new Regex($@"{regex}"), destinationFolder);
                var actualResult = _service.GetData(rule, fileName, 0);

                Assert.Equal(RULE_EXISTS_MESSAGE, actualResult.Message);
                Assert.Equal($"{destinationFolder}\\{fileName}", actualResult.Path);

                _nameBuilder.Verify(mock => mock.GetFinalName(fileName, 0), Times.Once);
            }

            [Theory]
            [InlineData("test.txt", ".line", "C:\\temp")]
            public void GetData_RuleNotExists(string fileName, string regex, string destinationFolder)
            {
                _nameBuilder.Setup(s => s.GetFinalName(It.IsAny<string>(), It.IsAny<int>())).Returns(fileName);

                var rule = new KeyValuePair<Regex, string>(new Regex($@"{regex}"), destinationFolder);
                var actualResult = _service.GetData(rule, fileName, 0);

                Assert.Equal($"{RULE_NOT_EXISTS_MESSAGE}\\{fileName} ", actualResult.Message);
                Assert.Equal($"{DEFAULT_FOLDER_PATH}\\{fileName}", actualResult.Path);

                _nameBuilder.Verify(mock => mock.GetFinalName(fileName, 0), Times.Once);
            }
        }
    }
}
